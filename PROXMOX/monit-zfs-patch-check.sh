#!/bin/sh
#                                                                                #
# ███████╗██╗    ██╗███████╗███████╗████████╗ ██████╗  ██████╗  ██████╗ ██████╗  #
# ██╔════╝██║    ██║██╔════╝██╔════╝╚══██╔══╝██╔════╝ ██╔═══██╗██╔═══██╗██╔══██╗ #
# ███████╗██║ █╗ ██║█████╗  █████╗     ██║   ██║  ███╗██║   ██║██║   ██║██║  ██║ #
# ╚════██║██║███╗██║██╔══╝  ██╔══╝     ██║   ██║   ██║██║   ██║██║   ██║██║  ██║ #
# ███████║╚███╔███╔╝███████╗███████╗   ██║   ╚██████╔╝╚██████╔╝╚██████╔╝██████╔╝ #
# ╚══════╝ ╚══╝╚══╝ ╚══════╝╚══════╝   ╚═╝    ╚═════╝  ╚═════╝  ╚═════╝ ╚═════╝  #
#                                                                                #
#                                  IT-Beratung für Datensicherheit & Open Source #
#                                                                                #
#                              sweetgood.de                                      #
#                                                                                #
# Copyright        : All rights reserved!
# Repository url   : https://codeberg.org/SWEETGOOD/shell-scripts/src/branch/main/PROXMOX
# Author           : codiflow
# Filename         : monit-zfs-patch-check.sh
# Created at       : 2024-11-28
# Last changed at  : 2024-11-28
# Input data       : Some files
# Output data      : Exit code which shows whether ZFSPoolPlugin.pm is patched or not
# Description      : Checks if ZFSPoolPlugin.pm is patched to support migrations
#                    and replication of encrypted ZFS datasets
#                    => To be used with monit (https://mmonit.com/monit/download)
#
# The script is meant to be used with Monit and is referenced in this PROXMOX thread:
# https://forum.proxmox.com/threads/allow-migration-and-replication-of-disks-on-zfs-encrypted-storage.117227/
#
# Credits          : Patch originally from selbitschka (https://forum.proxmox.com/threads/allow-migration-and-replication-of-disks-on-zfs-encrypted-storage.117227/post-520078)

###############
## VARIABLES ##
###############

# The path where the patch file should be written to
PATH_TO_PATCH="/root/ZFSPoolPlugin.pm.patch"

# The path where you put the backup file of the non-patched production version of ZFSPoolPlugin.pm
PATH_TO_BACKUP_FILE="/usr/share/perl5/PVE/Storage/ZFSPoolPlugin.pm.backup"

# The patch itself
PATCH="755,761c755
<     my \$cmd = ['zfs', 'send'];
<     my \$encrypted = \$class->zfs_get_properties(\$scfg, 'encryption', \"\$scfg->{pool}/\$dataset\");
<     if (\$encrypted !~ m/^off$/) {
<         push @\$cmd, '-Rpvw';
<     } else {
<         push @\$cmd, '-Rpv';
<     }
---
>     my \$cmd = ['zfs', 'send', '-Rpv'];
817,829d810
<     }
<     ### set key location and load key
<     my \$encrypted = \$class->zfs_get_properties(\$scfg, 'encryption', \$zfspath);
<     if (\$encrypted !~ m/^off$/) {
<         my \$keystatus = \$class->zfs_get_properties(\$scfg, 'keystatus', \$zfspath);
<         if (\$keystatus eq \"unavailable\") {
<             my (\$parent) = \$zfspath =~ /(.*)\/.*$/;
<             my \$keylocation = \$class->zfs_get_properties(\$scfg, 'keylocation', \$parent);
<             my \$keyformat = \$class->zfs_get_properties(\$scfg, 'keyformat', \$parent);
<             eval { run_command(['zfs', 'set', \"keylocation=\$keylocation\", \$zfspath]) };
<             eval { run_command(['zfs', 'set', \"keyformat=\$keyformat\", \$zfspath]) };
<             eval { run_command(['zfs', 'load-key', \$zfspath]) };
<         }"

############
## SCRIPT ##
############

### No changes are necessary below this line ###

# Root check
is_user_root () {
        [ "${EUID:=$(id -u)}" -eq 0 ]
}
if ! is_user_root; then
        echo "Please run the script as root."
        exit 1
fi

# Write patch to file if the file does not exist
if ! [ -f "${PATH_TO_PATCH}" ]; then
        echo "Writing patch file as it does not exist yet."
        echo "${PATCH}" > "${PATH_TO_PATCH}"
fi

# Check if backup file exists
if ! [ -f "${PATH_TO_BACKUP_FILE}" ]; then
        echo "The backup file '${PATH_TO_BACKUP_FILE}' does not exist. Please create it by duplicating the unpatched file."
        exit 1
fi

# Compare the production and the backup file
CHECK=$(diff /usr/share/perl5/PVE/Storage/ZFSPoolPlugin.pm ${PATH_TO_BACKUP_FILE})

if [ "${CHECK}" = "${PATCH}" ]; then
        echo "Everything is OK"
else
        echo "!!! Warning !!!"
        echo "ZFS replication and migration will NOT work because of unpatched ZFSPoolPlugin.pm"
        echo " "
        echo  "You can manually fix this issue by executing 'patch /usr/share/perl5/PVE/Storage/ZFSPoolPlugin.pm ${PATH_TO_PATCH}'"
        echo "Remember to do so on ALL nodes inside your cluster which send/receive encrypted ZFS datasets."

        # If you want to have the patch applied automatically in this case just uncomment the following line (AT YOUR OWN RISK!)
        #patch --batch /usr/share/perl5/PVE/Storage/ZFSPoolPlugin.pm "${PATH_TO_PATCH}"
        exit 1
fi
