#!/bin/sh
#                                                                                #
# ███████╗██╗    ██╗███████╗███████╗████████╗ ██████╗  ██████╗  ██████╗ ██████╗  #
# ██╔════╝██║    ██║██╔════╝██╔════╝╚══██╔══╝██╔════╝ ██╔═══██╗██╔═══██╗██╔══██╗ #
# ███████╗██║ █╗ ██║█████╗  █████╗     ██║   ██║  ███╗██║   ██║██║   ██║██║  ██║ #
# ╚════██║██║███╗██║██╔══╝  ██╔══╝     ██║   ██║   ██║██║   ██║██║   ██║██║  ██║ #
# ███████║╚███╔███╔╝███████╗███████╗   ██║   ╚██████╔╝╚██████╔╝╚██████╔╝██████╔╝ #
# ╚══════╝ ╚══╝╚══╝ ╚══════╝╚══════╝   ╚═╝    ╚═════╝  ╚═════╝  ╚═════╝ ╚═════╝  #
#                                                                                #
#                                  IT-Beratung für Datensicherheit & Open Source #
#                                                                                #
#                                  sweetgood.de                                  #
#                                                                                #
# Copyright        : All rights reserved!
# Repository url   : https://codeberg.org/SWEETGOOD/shell-scripts/src/branch/main/PROXMOX
# Author           : SWEETGOOD
# Filename         : monit-zfs-get-properties.sh
# Created at       : 28.11.2024
# Last changed at  : 07.01.2025
# Version          : 1.1.1
# Input data       : ZFS dataset properties
# Output data      : Count of unencrypted entries as exit code
# Description      : Automatically checks for unencrypted entries within a ZFS dataset
#                    recursively and reports the count as an exit code
#                    => To be used with monit (https://mmonit.com/monit/download)
#
# The script is meant to be used with Monit and is referenced in this PROXMOX thread:
# https://forum.proxmox.com/threads/allow-migration-and-replication-of-disks-on-zfs-encrypted-storage.117227/

# Get ZFS properties name,keylocation and encryptionroot recursively and check for "none"
# => This is used for displaying the findings within monit
echo "> bittank/encrypted"
zfs get -d 2 name,keylocation,encryptionroot bittank/encrypted | grep none
echo
echo "> clustertank/encrypted"
zfs get -d 2 name,keylocation,encryptionroot clustertank/encrypted | grep none

# If you need some excludes you can set it up like this
ADDME=0

# Count fleece entries (relevant for backup speedup)
FLEECE=$(zfs get -d 2 name,keylocation,encryptionroot clustertank/encrypted | grep -c fleece)

CHECK=$(zfs get -d 2 name,keylocation,encryptionroot bittank/encrypted | grep none)

if [ $(echo "${CHECK}" | grep -c "subvol-500") = 1 ] &&
[ $(echo "${CHECK}" | grep -c "vm-104") = 3 ] &&
[ $(echo "${CHECK}" | grep -c "vm-105") = 4 ] &&
[ $(echo "${CHECK}" | grep -c "vm-108") = 1 ] &&
[ $(echo "${CHECK}" | grep -c "vm-118") = 1 ] &&
[ $(echo "${CHECK}" | grep -c "vm-119") = 1 ] &&
[ $(echo "${CHECK}" | grep -c "vm-120") = 1 ] &&
[ $(echo "${CHECK}" | grep -c "vm-200") = 1 ] &&
[ $(echo "${CHECK}" | grep -c "vm-509") = 1 ] &&
[ $(echo "${CHECK}" | grep -c "vm-510") = 1 ] &&
[ $(echo "${CHECK}" | grep -c "vm-514") = 1 ] &&
[ $(echo "${CHECK}" | grep -c "vm-515") = 1 ]; then
        ADDME=20 # 17-3 (Skipping the two entries on bittank/encrypted adds 2 here and the single entry on clustertank/encrypted adds 1 here)
        echo "Skipping ${ADDME} entries - OK"
else
        echo "There is something wrong with the 'to skip' entries"
        ADDME=0
fi

exit $(($(zfs get -d 2 name,keylocation,encryptionroot bittank/encrypted | grep -c none) + $(zfs get -d 2 name,keylocation,encryptionroot clustertank/encrypted | grep -c none) - ADDME - FLEECE/3))