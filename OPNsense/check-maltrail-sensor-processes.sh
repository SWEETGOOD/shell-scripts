#!/bin/sh
COUNT=$(ps -awx -l | grep sensor.py | grep -v grep | wc -l | xargs)
echo "Currently running maltrail sensor.py process(es): ${COUNT}"
return ${COUNT}