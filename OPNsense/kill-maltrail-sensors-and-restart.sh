#!/bin/sh

#                                                                                #
# ███████╗██╗    ██╗███████╗███████╗████████╗ ██████╗  ██████╗  ██████╗ ██████╗  #
# ██╔════╝██║    ██║██╔════╝██╔════╝╚══██╔══╝██╔════╝ ██╔═══██╗██╔═══██╗██╔══██╗ #
# ███████╗██║ █╗ ██║█████╗  █████╗     ██║   ██║  ███╗██║   ██║██║   ██║██║  ██║ #
# ╚════██║██║███╗██║██╔══╝  ██╔══╝     ██║   ██║   ██║██║   ██║██║   ██║██║  ██║ #
# ███████║╚███╔███╔╝███████╗███████╗   ██║   ╚██████╔╝╚██████╔╝╚██████╔╝██████╔╝ #
# ╚══════╝ ╚══╝╚══╝ ╚══════╝╚══════╝   ╚═╝    ╚═════╝  ╚═════╝  ╚═════╝ ╚═════╝  #
#                                                                                #
#                                IT-Beratung für Datensicherheit und Open Source #
#                                                                                #
#                                  sweetgood.de                                  #
#                                                                                #
# Copyright        : All rights reserved!
# Repository url   : https://codeberg.org/SWEETGOOD/shell-scripts
# Author           : SWEETGOOD
# Filename         : kill-maltrail-sensors-and-restart.sh
# Created at       : 2024-10-21
# Last changed at  : 2024-10-21
# Version          : 1.0
# Input data       : PIDs
# Output data      : Restart of maltrail service
# Description      : This script gets the parent PIDs of all running maltrail
#                    sensors, kills them and restarts the whole maltrail process
#
#                    This script is meant to be used on OPNsense with Monit and
#                    has been tested with OPNsense 24.1.10_8 using Maltrail 0.65

# Stop maltrail SERVER
/usr/local/sbin/configctl maltrailserver stop

# Stop maltrail SENSOR
/usr/local/sbin/configctl maltrailsensor stop

# Get all maltrail SENSOR PIDs
for processid in $(ps -awx -l | grep sensor.py | grep -v grep | awk '{print $2}'); do
    # Check whether sensor process has children
    if pgrep -lP "$processid"; then
        echo "Process ID $processid has children, killing parent"
        # Kill parent process
        kill "$processid"
    fi
done

PIDS=$(ps -awx -l | grep sensor.py | grep -v grep | awk '{print $2}' | tr '\n' ' ')

# Get all maltrail SENSOR PIDs (should be none now)
if ! ps -awx -l | grep sensor.py | grep -v grep | awk '{print $2}'; then
    echo "There are still the following sensor.py processes running: ${PIDS}"
fi

# Kill remaining SENSOR PIDs
kill ${PIDS}

echo "Sensor cleanup successful, restarting maltrail"

# Start maltrail SERVER
/usr/local/sbin/configctl maltrailserver start

# Start initial maltrail SENSOR process
/usr/local/sbin/configctl maltrailsensor start