# Shell scripts

> My shell scripts which might be helpful for others. Mostly they are referenced in one of [my blog articles](https://andersgood.de) or within forum threads.

> NOTE:  
> They come with **absolutely NO WARRANTY**. If you need help feel free to reach out to me at [sweetgood.de/contact](https://sweetgood.de/en/contact)

---

## Miscellaneous

### parse-raw-smart-values-seagate.sh
<details>
<summary>Show usage and description</summary>
<br />

Download: [parse-raw-smart-values-seagate.sh](https://codeberg.org/SWEETGOOD/shell-scripts/raw/branch/main/parse-raw-smart-values-seagate.sh)  
Usage: `./parse-raw-smart-values-seagate.sh sda`

Example output:
```
Device Model: ST4000VN006-3CW104n
Serial Number: WW20YRWBm
Raw_Read_Error_Rate: 0 errors in 240.011.568 operations.
Seek_Error_Rate: 0 errors in 25.222.805 operations.
Hardware_ECC_Recovered: 0 errors in 240.018.104 operations.
```

Exit codes:
```
1: First value of any of the three parameters is not 0
0: All other cases
```

🐶 Script can easily be used with [monit](https://mmonit.com/monit/):

```
check program sda-seagate-check with path "/path/to/script/parse-raw-smart-values-seagate.sh sda"
    every 60 cycles
    if status != 0 then alert
```

✅ Shellcheck: **100%**
</details>

### humhub-create-staging-environment.sh
<details>
<summary>Show usage and description</summary>
<br />

Download: [humhub-create-staging-environment.sh](https://codeberg.org/SWEETGOOD/shell-scripts/raw/branch/main/humhub-create-staging-environment.sh)  
Usage: `./humhub-create-staging-environment.sh`

Before using this script [read the official tutorial](https://community.humhub.com/s/installation-and-setup/wiki/136/Staging+Environment).

✅ Shellcheck: **100%**
</details>

---

## OPNsense
![](https://codeberg.org/SWEETGOOD/shell-scripts/raw/branch/main/logos/opnsense-300.png)

### check-maltrail-sensor-processes.sh
<details>
<summary>Show usage and description</summary>
<br />

Download: [check-maltrail-sensor-processes.sh](https://codeberg.org/SWEETGOOD/shell-scripts/src/branch/main/OPNsense/check-maltrail-sensor-processes.sh)  
Usage: `./check-maltrail-sensor-processes.sh`

Before using this script [read the German blog article](https://andersgood.de/blog/oeffentliche-ip-adressen-in-opnsense-mit-maltrail-absichern).
</details>

### kill-maltrail-sensors-and-restart.sh
<details>
<summary>Show usage and description</summary>
<br />

This script gets the parent PIDs of all running maltrail sensors, kills them and restarts the whole maltrail process.

This script is meant to be used on OPNsense with Monit and has been tested with OPNsense 24.1.10_8 using Maltrail 0.65.

Download: [kill-maltrail-sensors-and-restart.sh](https://codeberg.org/SWEETGOOD/shell-scripts/src/branch/main/OPNsense/kill-maltrail-sensors-and-restart.sh)  
Usage: `./kill-maltrail-sensors-and-restart.sh`

Before using this script [read the German blog article](https://andersgood.de/blog/oeffentliche-ip-adressen-in-opnsense-mit-maltrail-absichern).
</details>

---

## PROXMOX
![](https://codeberg.org/SWEETGOOD/shell-scripts/raw/branch/main/logos/proxmox-300.png)

### monit-zfs-get-properties.sh
<details>
<summary>Show usage and description</summary>
<br />

Automatically checks for unencrypted entries within a ZFS dataset recursively and reports the count as an exit code.

The script is meant to be used with [Monit](https://mmonit.com/monit/download).

Download: [monit-zfs-get-properties.sh](https://codeberg.org/SWEETGOOD/shell-scripts/src/branch/main/PROXMOX/monit-zfs-get-properties.sh)  
Usage: `./monit-zfs-get-properties.sh`

Before using this script [read the PROXMOX forum thread](https://forum.proxmox.com/threads/allow-migration-and-replication-of-disks-on-zfs-encrypted-storage.117227/).
</details>

### monit-zfs-patch-check.sh
<details>
<summary>Show usage and description</summary>
<br />

Checks if ZFSPoolPlugin.pm is patched to support migrations and replication of encrypted ZFS datasets.

The script is meant to be used with [Monit](https://mmonit.com/monit/download).

Download: [monit-zfs-patch-check.sh](https://codeberg.org/SWEETGOOD/shell-scripts/src/branch/main/PROXMOX/monit-zfs-patch-check.sh)  
Usage: `./monit-zfs-patch-check.sh`

Before using this script [read the PROXMOX forum thread](https://forum.proxmox.com/threads/allow-migration-and-replication-of-disks-on-zfs-encrypted-storage.117227/).
</details>