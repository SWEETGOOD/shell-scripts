#                                                                                #
# ███████╗██╗    ██╗███████╗███████╗████████╗ ██████╗  ██████╗  ██████╗ ██████╗  #
# ██╔════╝██║    ██║██╔════╝██╔════╝╚══██╔══╝██╔════╝ ██╔═══██╗██╔═══██╗██╔══██╗ #
# ███████╗██║ █╗ ██║█████╗  █████╗     ██║   ██║  ███╗██║   ██║██║   ██║██║  ██║ #
# ╚════██║██║███╗██║██╔══╝  ██╔══╝     ██║   ██║   ██║██║   ██║██║   ██║██║  ██║ #
# ███████║╚███╔███╔╝███████╗███████╗   ██║   ╚██████╔╝╚██████╔╝╚██████╔╝██████╔╝ #
# ╚══════╝ ╚══╝╚══╝ ╚══════╝╚══════╝   ╚═╝    ╚═════╝  ╚═════╝  ╚═════╝ ╚═════╝  #
#                                                                                #
#                                      IT-Beratung mit Fokus auf Datensicherheit #
#                                                                                #
#                            www.sweetgood.de                                    #
#                                                                                #
# Copyright        : All rights reserved!
# Repository url   : https://codeberg.org/SWEETGOOD/shell-scripts
# Author           : SWEETGOOD
# Filename         : humhub-create-staging-environment.sh
# Created at       : 19.05.2024
# Last changed at  : 19.05.2024
# Version          : 1.0
# Description      : Creates a staging environment from a production installation of HumHub
#                    Based on Lucas Bartholemy's work (Thanks!)
# Original source  : https://community.humhub.com/s/installation-and-setup/wiki/136/Staging+Environment

#!/bin/sh -e

# CONFIGURATION
# Please adjust all the variables according to your needs
PATH_TO_PHP_BINARY="/usr/bin/php8.1"

STAGING_URL="https://staging.domain.de"
STAGING_PATH="/var/www/html/staging.domain.de" # NO trailing slash
STAGING_POOL_USER="www-data-staging"
STAGING_POOL_GROUP="www-data-staging"
STAGING_DB_NAME="humhub_staging"
STAGING_DB_USER="humhub_stag_user"
STAGING_DB_PASSWORD="XXX"
STAGING_DB_HOST="localhost"
STAGING_NAME="HumHub STAGING ENVIRONMENT"

PRODUCTION_PATH="/var/www/html/domain.de/htdocs" # NO trailing slash
PRODUCTION_DB_NAME="humhub_production"
PRODUCTION_DB_USER="humhub_prod_user"
PRODUCTION_DB_PASSWORD="XXX"

# --- NORMALLY NOTHING NEEDS TO BE CHANGED BELOW THIS LINE ---

RED='\e[31m'
MAGENTA='\e[35m'
BLUE='\e[36m'
GREEN='\e[0;32m'
NC='\e[39m' # No Color

# Disable Cronjobs
echo "${BLUE}Disabling cronjobs...${NC}"
crontab -l -u "${STAGING_POOL_USER}" >"/tmp/${STAGING_POOL_USER}.cron.tmp"
crontab -r -u "${STAGING_POOL_USER}"

# Copy database
echo "${BLUE}Copying database...${NC}"
mysql -e "DROP DATABASE IF EXISTS \`${STAGING_DB_NAME}\`"
mysql -e "CREATE DATABASE \`${STAGING_DB_NAME}\` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
mysql -e "GRANT ALL ON \`${STAGING_DB_NAME}\`.* TO \`${STAGING_DB_USER}\`@${STAGING_DB_HOST}"

mysqldump "${PRODUCTION_DB_NAME}" | mysql "${STAGING_DB_NAME}"

# Copy files
echo "${BLUE}Copying files...${NC}"
rm -rf "${STAGING_PATH}"
cp -rfa "${PRODUCTION_PATH}" "${STAGING_PATH}"

# Adjust HumHub configuration files
echo "${BLUE}Adjusting configuration files...${NC}"
sed -i "s/${PRODUCTION_DB_NAME}/${STAGING_DB_NAME}/g" "${STAGING_PATH}/protected/config/dynamic.php"
sed -i "s/${PRODUCTION_DB_USER}/${STAGING_DB_USER}/g" "${STAGING_PATH}/protected/config/dynamic.php"

ESCAPED_DB_PASS_PROD=$(printf '%s\n' "${PRODUCTION_DB_PASSWORD}" | sed -e 's/[\/&]/\\&/g')
ESCAPED_DB_PASS_STAGE=$(printf '%s\n' "${STAGING_DB_PASSWORD}" | sed -e 's/[\/&]/\\&/g')
sed -i "s/${ESCAPED_DB_PASS_PROD}/${ESCAPED_DB_PASS_STAGE}/g" "${STAGING_PATH}/protected/config/dynamic.php"

# Update configuration table
echo "${BLUE}Updating configuration table...${NC}"
"${PATH_TO_PHP_BINARY}" "${STAGING_PATH}/protected/yii" settings/set base "name" "${STAGING_NAME}"
"${PATH_TO_PHP_BINARY}" "${STAGING_PATH}/protected/yii" settings/set base "mailer.transportType" "file"
"${PATH_TO_PHP_BINARY}" "${STAGING_PATH}/protected/yii" settings/set base "baseUrl" "${STAGING_URL}"

# Cleanup
echo "${BLUE}Cleanup...${NC}"
rm -rf "${STAGING_PATH}/protected/runtime/cache"
chown -R "${STAGING_POOL_USER}":"${STAGING_POOL_GROUP}" "${STAGING_PATH}"

# Enable Cronjobs
echo "${BLUE}Enabling cronjobs...${NC}"
crontab -u "${STAGING_POOL_USER}" "/tmp/${STAGING_POOL_USER}.cron.tmp"

echo "${GREEN}Finished!${NC}"
